#!/bin/bash

cd "$(dirname "$0")"
cd ..
workspace_dir=$PWD
arch="$(uname -m)"

if [ "$(docker ps -aq -f status=exited -f name=rtp2ros)" ]; then
    docker rm ubuntu_test_ci;
fi

docker run -it -d --rm \
    --name ubuntu_test_ci \
    --net "host" \
    -v $workspace_dir/:/home/docker_test_ci/catkin_ws/src:rw \
    ubuntu_test_ci:latest